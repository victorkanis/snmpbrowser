﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace snmpBrowser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly BackgroundWorker worker = new BackgroundWorker();

        public MainWindow()
        {
            InitializeComponent();
            txtHostName.Text = ConfigUtils.GetConfig("HostName");
            txtPassword.Text = ConfigUtils.GetConfig("Password");

            worker.DoWork += worker_DoWork;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtHostName.Text = string.Empty;
            txtPassword.Text = string.Empty;
            SetStatusText("Ready");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (TextUtils.IsHostName(txtHostName.Text) && 
                TextUtils.HasText(txtPassword.Text))
            {
                // listViewResult.ItemsSource = SnmpUtil.PollHost(txtHostName.Text, txtPassword.Text).Tables[0].DefaultView;
                ConfigUtils.SaveConfig("HostName", txtHostName.Text);
                ConfigUtils.SaveConfig("Password", txtPassword.Text);

                listViewResult.ItemsSource = null;

                SetStatusText(string.Format("Getting values from {0}", txtHostName.Text));
                progressBar.IsIndeterminate = true;
                string[] vals = new string[] { txtHostName.Text, txtPassword.Text };
                worker.RunWorkerAsync(vals);
            }
            else
            {
                txtStatusText.Text = "Validate hostname and password values";
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        private void SetStatusText(string s)
        {
            txtStatusText.Text = s;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItemExit_Click(object sender, RoutedEventArgs e)
        {
            SetStatusText("Exit");
            this.Close();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            AboutWindow aw = new AboutWindow();
            aw.Show();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string[] a = (string[])e.Argument;  
            e.Result = SnmpUtil.PollHost(a[0], a[1]).Tables[0].DefaultView;                
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // update ui once worker complete his work
            progressBar.IsIndeterminate = false;
            if (e.Error != null)
            {
                SetStatusText(e.Error.Message);
            }
            else
            {
                if (((System.Data.DataView)(e.Result)).Count > 0)
                {
                    SetStatusText("Ready");
                }
                else
                {
                    SetStatusText("No results");
                }
            }
            if (e.Result != null)
            {
                // List<ShippingLabel> l = (List<ShippingLabel>)e.Result;
                listViewResult.ItemsSource = (System.Data.DataView)e.Result;
            }
        }
    }
}
