# README #

A simple C# WPF application to read SNMP data. Project based on an old freelancer.com posting that I saw. Thought it might be fun to see if I could do it.

Originally built in VS 2012 on Win7, has recently been tested against VS 2015 on Win10

See https://code.circayou.com/application-portfolio-snmp-browser-demo/ for an overview

![snmpDemoMainScreen.png](https://bitbucket.org/repo/9kLo9M/images/2640463608-snmpDemoMainScreen.png)