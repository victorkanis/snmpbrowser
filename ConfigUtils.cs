﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snmpBrowser
{
    public class ConfigUtils
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        internal static string GetConfig (string key)
        {
            return GetConfig(key, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="toUpper"></param>
        /// <returns></returns>
        internal static string GetConfig(string key, bool toUpper)
        {
            string r = string.Empty;
            try
            {
                //r = ConfigurationManager.AppSettings[key].ToString();
                r = Properties.Settings.Default[key].ToString();
                if (toUpper)
                {
                    r = r.ToUpper();
                }
            }
            catch (Exception ex)
            {
                // logging!
            }
            return r;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        internal static void SaveConfig(string key, string val)
        {
            //ConfigurationManager.AppSettings.Set(key, val);
            Properties.Settings.Default[key] = val;
            Properties.Settings.Default.Save();
        }
    }
}
